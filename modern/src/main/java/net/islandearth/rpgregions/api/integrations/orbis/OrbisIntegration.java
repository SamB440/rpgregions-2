package net.islandearth.rpgregions.api.integrations.orbis;

import net.islandearth.rpgregions.api.IRPGRegionsAPI;
import net.islandearth.rpgregions.api.events.RegionsEnterEvent;
import net.islandearth.rpgregions.api.integrations.IntegrationManager;
import net.islandearth.rpgregions.managers.data.region.ConfiguredRegion;
import net.kyori.adventure.key.Key;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerMoveEvent;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.empirewar.orbis.OrbisAPI;
import org.empirewar.orbis.query.RegionQuery;
import org.empirewar.orbis.region.Region;
import org.empirewar.orbis.world.RegionisedWorld;
import org.jetbrains.annotations.NotNull;
import org.joml.Vector3ic;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class OrbisIntegration implements IntegrationManager {

    private final IRPGRegionsAPI plugin;

    public OrbisIntegration(IRPGRegionsAPI plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean isInRegion(Location location) {
        return !this.getRegions(location).isEmpty();
    }

    @Override
    public void handleMove(PlayerMoveEvent pme) {
        if (pme.getTo() == null) return;

        plugin.debug("Handling movement");
        Player player = pme.getPlayer();
        int oldX = pme.getFrom().getBlockX();
        int oldY = pme.getFrom().getBlockY();
        int oldZ = pme.getFrom().getBlockZ();
        int x = pme.getTo().getBlockX();
        int y = pme.getTo().getBlockY();
        int z = pme.getTo().getBlockZ();
        Set<Region> oldRegions = this.getRegions(new Location(player.getWorld(), oldX, oldY, oldZ));
        Set<Region> regions = this.getRegions(new Location(player.getWorld(), x, y, z));

        Optional<ConfiguredRegion> configuredRegion = getPrioritisedRegion(pme.getTo());
        configuredRegion.ifPresent(prioritisedRegion -> {
            plugin.debug("Priority region found");
            plugin.debug("Old: " + oldRegions);
            plugin.debug("New: " + regions);
            List<String> stringRegions = new ArrayList<>();
            regions.forEach(region -> {
                if (!prioritisedRegion.getId().equals(region.name())
                        && checkRequirements(pme, prioritisedRegion, region.name())) stringRegions.add(region.name());
            });

            if (checkRequirements(pme, prioritisedRegion, prioritisedRegion.getId())) {
                plugin.debug("Requirements passed, calling");
                stringRegions.add(0, prioritisedRegion.getId());
                Bukkit.getPluginManager().callEvent(new RegionsEnterEvent(player, stringRegions, !oldRegions.equals(regions)));
            }
        });
    }

    @Override
    public Optional<ConfiguredRegion> getPrioritisedRegion(Location location) {
        Set<Region> regions = this.getRegions(location);
        Region highest = null;
        for (Region region : regions) {
            if (highest == null) {
                highest = region;
                continue;
            }

            if (region.priority() >= highest.priority()) {
                highest = region;
            }
        }

        if (highest == null) return Optional.empty();
        return plugin.getManagers().getRegionsCache().getConfiguredRegion(highest.name());
    }

    @Override
    public boolean exists(World world, String region) {
        final RegionisedWorld regionisedWorld = OrbisAPI.get().getRegionisedWorld(worldToKey(world));
        return regionisedWorld.getByName(region).isPresent();
    }

    @Override
    public Set<String> getAllRegionNames(World world) {
        final RegionisedWorld regionisedWorld = OrbisAPI.get().getRegionisedWorld(worldToKey(world));
        return regionisedWorld.regions().stream().map(Region::name).collect(Collectors.toSet());
    }

    @Override
    @NonNull public List<Location> getBoundingBoxPoints(@NotNull ConfiguredRegion region) {
        List<Location> points = new ArrayList<>();
        final RegionisedWorld world = OrbisAPI.get().getRegionisedWorld(worldToKey(region.getWorld()));
        final Region protectedRegion = world.getByName(region.getId()).orElseThrow();
        for (Vector3ic point : protectedRegion.area().points()) {
            points.add(new Location(region.getWorld(), point.x(), region.getLocation().getY(), point.z()));
        }
        return points;
    }

    private Set<Region> getRegions(Location location) {
        final RegionisedWorld world = OrbisAPI.get().getRegionisedWorld(worldToKey(location.getWorld()));
        return world.query(RegionQuery.Position.builder()
                .position(location.getX(), location.getY(), location.getZ()).build())
                .result();
    }

    private Key worldToKey(World world) {
        return Key.key(world.getKey().toString());
    }
}
