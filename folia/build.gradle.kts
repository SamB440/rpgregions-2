plugins {
    id("buildlogic.java-common-conventions")
}

java {
    disableAutoTargetJvm()
}

dependencies {
    compileOnly(project(":api"))

    compileOnly("dev.folia:folia-api:${properties["folia_version"]}")
}
