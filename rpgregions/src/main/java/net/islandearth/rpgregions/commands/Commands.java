package net.islandearth.rpgregions.commands;

import com.github.benmanes.caffeine.cache.Caffeine;
import io.leangen.geantyref.TypeFactory;
import io.leangen.geantyref.TypeToken;
import net.islandearth.rpgregions.RPGRegions;
import net.islandearth.rpgregions.api.integrations.rpgregions.RPGRegionsIntegration;
import net.islandearth.rpgregions.api.integrations.rpgregions.region.RPGRegionsRegion;
import net.islandearth.rpgregions.commands.caption.RPGRegionsCaptionProvider;
import net.islandearth.rpgregions.commands.parser.ConfiguredRegionArgument;
import net.islandearth.rpgregions.commands.parser.FaunaArgument;
import net.islandearth.rpgregions.commands.parser.IntegrationRegionArgument;
import net.islandearth.rpgregions.fauna.FaunaInstance;
import net.islandearth.rpgregions.managers.data.region.ConfiguredRegion;
import net.islandearth.rpgregions.utils.Colors;
import net.kyori.adventure.audience.Audience;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.incendo.cloud.SenderMapper;
import org.incendo.cloud.annotations.AnnotationParser;
import org.incendo.cloud.bukkit.CloudBukkitCapabilities;
import org.incendo.cloud.execution.ExecutionCoordinator;
import org.incendo.cloud.minecraft.extras.MinecraftExceptionHandler;
import org.incendo.cloud.paper.LegacyPaperCommandManager;
import org.incendo.cloud.parser.ParserRegistry;
import org.incendo.cloud.processors.cache.CaffeineCache;
import org.incendo.cloud.processors.confirmation.ConfirmationConfiguration;
import org.incendo.cloud.processors.confirmation.ConfirmationManager;
import org.incendo.cloud.suggestion.Suggestion;

import java.io.File;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Stream;

import static net.kyori.adventure.text.Component.text;

public class Commands {

    private ConfirmationManager<CommandSender> confirmationManager;

    public Commands(RPGRegions plugin) {

        final LegacyPaperCommandManager<CommandSender> manager;
        try {
            manager = new LegacyPaperCommandManager<>(
                    plugin,
                    ExecutionCoordinator.simpleCoordinator(),
                    SenderMapper.identity()
            );
        } catch (Exception e) {
            plugin.getLogger().severe("Failed to initialize the command manager");
            e.printStackTrace();
            return;
        }

        // Register Brigadier mappings or asynchronous completions
        if (manager.hasCapability(CloudBukkitCapabilities.NATIVE_BRIGADIER)) {
            manager.registerBrigadier();
        } else if (manager.hasCapability(CloudBukkitCapabilities.ASYNCHRONOUS_COMPLETION)) {
            manager.registerAsynchronousCompletions();
        }

        /*
         * Create the confirmation manager. This allows us to require certain commands to be
         * confirmed before they can be executed
         */
        ConfirmationConfiguration<CommandSender> confirmationConfig = ConfirmationConfiguration.<CommandSender>builder()
                .cache(CaffeineCache.of(Caffeine.newBuilder().expireAfterWrite(Duration.ofSeconds(30)).build()))
                .noPendingCommandNotifier(sender -> {
                    final Audience audience = plugin.adventure().sender(sender);
                    audience.sendMessage(text("You don't have any pending confirmations.", Colors.BRIGHT_RED));
                })
                .confirmationRequiredNotifier((sender, context) -> {
                    final Audience audience = plugin.adventure().sender(sender);
                    audience.sendMessage(text("Confirmation required. Confirm using", Colors.BRIGHT_RED).append(text(" /rpgregions confirm", Colors.HONEY_YELLOW).append(text(".", Colors.BRIGHT_RED))));
                })
                .build();

        confirmationManager = ConfirmationManager.confirmationManager(confirmationConfig);

        // Register the confirmation command.
        manager.command(
                manager.commandBuilder("rpgregions")
                        .literal("confirm")
                        .handler(confirmationManager.createExecutionHandler())
        );

        // Register the confirmation processor. This will enable confirmations for commands that require it
        manager.registerCommandPostProcessor(confirmationManager.createPostprocessor());

        // Override the default exception handlers
        // todo: change some stuff lmao
        MinecraftExceptionHandler.<CommandSender>create(sender -> plugin.adventure().sender(sender))
                .defaultInvalidSyntaxHandler()
                .defaultInvalidSenderHandler()
                .defaultNoPermissionHandler()
                .defaultArgumentParsingHandler()
                .defaultCommandExecutionHandler()
                .registerTo(manager);

        // Register our custom caption registry so we can define exception messages for parsers
        manager.captionRegistry().registerProvider(new RPGRegionsCaptionProvider<>());

        // Register custom parsers
        final ParserRegistry<CommandSender> parserRegistry = manager.parserRegistry();
        parserRegistry.registerParserSupplier(TypeToken.get(ConfiguredRegion.class), parserParameters -> new ConfiguredRegionArgument<>());
        parserRegistry.registerParserSupplier(TypeToken.get(TypeFactory.parameterizedClass(FaunaInstance.class, TypeFactory.unboundWildcard())), parserParameters ->
                new FaunaArgument<>());
        parserRegistry.registerParserSupplier(TypeToken.get(RPGRegionsRegion.class), parserParameters ->
                new IntegrationRegionArgument<>());

        parserRegistry.registerSuggestionProvider("region-types", (context, arg) -> CompletableFuture.completedFuture(Stream.of("Cuboid", "Poly").map(Suggestion::suggestion).toList()));

        parserRegistry.registerSuggestionProvider("integration-regions", (context, arg) -> {
            if (context.sender() instanceof Player player) {
                return CompletableFuture.completedFuture(plugin.getManagers().getIntegrationManager()
                        .getAllRegionNames(player.getWorld()).stream().map(Suggestion::suggestion).toList());
            }
            return CompletableFuture.completedFuture(List.of());
        });

        parserRegistry.registerSuggestionProvider("templates", (context, arg) -> {
            File templates = new File(plugin.getDataFolder() + File.separator + "templates");
            List<Suggestion> files = new ArrayList<>();
            for (File file : templates.listFiles()) {
                files.add(Suggestion.suggestion(file.getName()));
            }
            return CompletableFuture.completedFuture(files);
        });

        parserRegistry.registerSuggestionProvider("schematics", (context, arg) -> {
            File schematicFolder = new File("plugins/WorldEdit/schematics/");
            List<Suggestion> files = new ArrayList<>();
            for (File file : schematicFolder.listFiles()) {
                files.add(Suggestion.suggestion(file.getName()));
            }
            return CompletableFuture.completedFuture(files);
        });

        parserRegistry.registerSuggestionProvider("async", (context, arg) -> CompletableFuture.completedFuture(List.of(Suggestion.suggestion("--async"))));

        final AnnotationParser<CommandSender> annotationParser = new AnnotationParser<>(manager, CommandSender.class);
        annotationParser.parse(new DiscoveriesCommand(plugin));
        annotationParser.parse(new RPGRegionsCommand(plugin, manager));
        annotationParser.parse(new RPGRegionsDebugCommand(plugin));
        annotationParser.parse(new RPGRegionsExportCommand(plugin, manager));
        if (plugin.getManagers().getIntegrationManager() instanceof RPGRegionsIntegration) {
            annotationParser.parse(new RPGRegionsIntegrationCommand(plugin, manager));
        }
    }
}
