package net.islandearth.rpgregions.commands;

import co.aikar.idb.DB;
import co.aikar.idb.Database;
import net.islandearth.rpgregions.RPGRegions;
import net.islandearth.rpgregions.effects.RegionEffect;
import net.islandearth.rpgregions.managers.data.IStorageManager;
import net.islandearth.rpgregions.requirements.RegionRequirement;
import net.islandearth.rpgregions.rewards.DiscoveryReward;
import net.islandearth.rpgregions.thread.Blocking;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.incendo.cloud.annotations.Argument;
import org.incendo.cloud.annotations.Command;
import org.incendo.cloud.annotations.Permission;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

@Permission("rpgregions.debug")
public class RPGRegionsDebugCommand {

    private final RPGRegions plugin;

    public RPGRegionsDebugCommand(final RPGRegions plugin) {
        this.plugin = plugin;
    }

    @Command("rpgregionsdebug|rpgrd")
    public void onDefault(CommandSender sender) throws SQLException {
        IStorageManager storageManager = plugin.getManagers().getStorageManager();
        sender.sendMessage(ChatColor.GOLD + "Database status:");
        sender.sendMessage(ChatColor.GRAY + "Storage implementation " + storageManager.getClass().getSimpleName() + ".");
        sender.sendMessage(ChatColor.GRAY + "" + storageManager.getCachedAccounts().synchronous().asMap().size()
                + " cached players.");
        final List<Long> timings = storageManager.getTimings();
        sender.sendMessage(ChatColor.GRAY + "Performance (" + storageManager.getTimingsAverage() + " avg. ms)" +
                " (last 3 retrievals): " + timings.get(0) + " ms, " + timings.get(1) + " ms, " + timings.get(2) + " ms");
        Database database = DB.getGlobalDatabase();
        if (database != null) {
            Connection connection = database.getConnection();
            sender.sendMessage(ChatColor.GRAY + "Product: " + connection.getMetaData().getDatabaseProductName());
            sender.sendMessage(ChatColor.GRAY + "Version: " + connection.getMetaData().getDatabaseProductVersion());
            sender.sendMessage(ChatColor.GRAY + "Driver: " + connection.getMetaData().getDriverName());
            sender.sendMessage(ChatColor.GRAY + "Version: " + connection.getMetaData().getDriverVersion());
            database.closeConnection(connection);
        }

        plugin.getManagers().getRegionsCache().getConfiguredRegions().forEach((name, configuredRegion) -> {
            for (DiscoveryReward reward : configuredRegion.getRewards()) {
                if (reward instanceof Blocking) {
                    sender.sendMessage(ChatColor.RED + "Region " + name + " has blocking class " + reward + ".");
                }
            }
            for (RegionRequirement requirement : configuredRegion.getRequirements()) {
                if (requirement instanceof Blocking) {
                    sender.sendMessage(ChatColor.RED + "Region " + name + " has blocking class " + requirement + ".");
                }
            }
            for (RegionEffect effect : configuredRegion.getEffects()) {
                if (effect instanceof Blocking) {
                    sender.sendMessage(ChatColor.RED + "Region " + name + " has blocking class " + effect + ".");
                }
            }
        });
    }

    @Command("rpgregionsdebug|rpgrd enable|disable|toggle")
    public void onEnable(CommandSender sender) {
        final boolean newValue = !plugin.debug();
        plugin.getConfig().set("settings.dev.debug", newValue);
        plugin.markDebugDirty();
        if (newValue) sender.sendMessage(ChatColor.GREEN + "Debug enabled.");
        else sender.sendMessage(ChatColor.RED + "Debug disabled.");
    }

    @Command("rpgregionsdebug|rpgrd cache")
    public void onCache(CommandSender sender) {
        IStorageManager storageManager = plugin.getManagers().getStorageManager();
        sender.sendMessage(ChatColor.GOLD + "Database cache:");
        storageManager.getCachedAccounts().synchronous().asMap().forEach((uuid, account) -> {
            sender.sendMessage(ChatColor.GRAY + " - " + uuid.toString());
        });
    }

    @Command("rpgregionsdebug|rpgrd removecached <player>")
    public void onRemoveCached(CommandSender sender, @Argument("player") OfflinePlayer player) {
        IStorageManager storageManager = plugin.getManagers().getStorageManager();
        sender.sendMessage(ChatColor.GREEN + "Removing from cache...");
        storageManager.removeCachedAccount(player.getUniqueId()).thenAccept((a) -> {
            sender.sendMessage(ChatColor.GREEN + "Successfully removed and saved " + player.getName() + "'s account from the cache.");
        });
    }

    @Command("rpgregionsdebug|rpgrd worldid <world>")
    public void onGetWorldId(CommandSender sender, @Argument("world") World world) {
        sender.sendMessage(ChatColor.GREEN + String.valueOf(world.getUID()));
    }
}
