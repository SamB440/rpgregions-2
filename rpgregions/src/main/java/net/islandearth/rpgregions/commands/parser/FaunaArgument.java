package net.islandearth.rpgregions.commands.parser;

import com.google.common.collect.ImmutableList;
import net.islandearth.rpgregions.api.RPGRegionsAPI;
import net.islandearth.rpgregions.commands.caption.RPGRegionsCaptionKeys;
import net.islandearth.rpgregions.fauna.FaunaInstance;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.incendo.cloud.caption.CaptionVariable;
import org.incendo.cloud.context.CommandContext;
import org.incendo.cloud.context.CommandInput;
import org.incendo.cloud.exception.parsing.ParserException;
import org.incendo.cloud.parser.ArgumentParseResult;
import org.incendo.cloud.parser.ArgumentParser;
import org.incendo.cloud.suggestion.BlockingSuggestionProvider;

import java.util.Optional;
import java.util.stream.Collectors;

public final class FaunaArgument<C> implements ArgumentParser<C, FaunaInstance<?>>, BlockingSuggestionProvider.Strings<C> {

    @Override
    public @NonNull ArgumentParseResult<@NonNull FaunaInstance<?>> parse(@NonNull CommandContext<@NonNull C> commandContext, @NonNull CommandInput commandInput) {
        final String input = commandInput.peekString();

        final Optional<FaunaInstance<?>> fauna = RPGRegionsAPI.getAPI().getManagers().getFaunaCache().getFauna(input);
        if (fauna.isPresent()) {
            commandInput.readString();
            return ArgumentParseResult.success(fauna.get());
        }
        return ArgumentParseResult.failure(new FaunaParserException(input, commandContext));
    }

    @Override
    public @NonNull Iterable<@NonNull String> stringSuggestions(@NonNull CommandContext<C> commandContext, @NonNull CommandInput input) {
        return ImmutableList.copyOf(RPGRegionsAPI.getAPI().getManagers().getFaunaCache().getFauna().stream().map(FaunaInstance::getIdentifier).collect(Collectors.toList()));
    }

    public static final class FaunaParserException extends ParserException {

        public FaunaParserException(
                final @NonNull String input,
                final @NonNull CommandContext<?> context
        ) {
            super(
                    FaunaArgument.class,
                    context,
                    RPGRegionsCaptionKeys.ARGUMENT_PARSE_FAILURE_FAUNA_NOT_FOUND,
                    CaptionVariable.of("input", input)
            );
        }
    }
}
